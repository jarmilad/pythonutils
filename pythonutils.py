# pythonutils

def sum_of_numbers(*numbers):
    res = 0
    for i in range(len(numbers)):
        res += numbers[i]
    return res


def subtraction_of_numbers(*numbers):
    res = numbers[0]
    for i in range(1, len(numbers)):
        res -= numbers[i]
    return res


def multiplication_of_numbers(*numbers):
    res = 1
    for i in range(len(numbers)):
        res *= numbers[i]
    return res


def division_of_numbers(num1, num2, real=False):
    if num2 == 0:
        print("Cannot divide by 0")
    elif not real:
        return num1 // num2
    else:
        return num1 / num2


def modulo_of_numbers(num1, num2):
    if num2 == 0:
        print("Cannot divide by 0")
    else:
        return num1 % num2


def factorial(num):
    res = 1
    for i in range(2, num + 1):
        res *= i
    return res


def digit_sum(num):
    res = 0
    while num != 0:
        res += num % 10
        num //= 10
    return res


def digit_count(num):
    res = 0
    if num == 0:
        return 1
    while num != 0:
        res += 1
        num //= 10
    return res
