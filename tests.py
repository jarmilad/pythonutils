import pythonutils

# pythonutils tests
print("1 + 2 + 3 = " + str(pythonutils.sum_of_numbers(1, 2, 3)))
print("3 - 2 - 1 = " + str(pythonutils.subtraction_of_numbers(3, 2, 1)))
print("5 * 2 * 3 = " + str(pythonutils.multiplication_of_numbers(5, 2, 3)))
print("4 / 3 = " + str(pythonutils.division_of_numbers(4, 3)))
print("1 / 0 = " + str(pythonutils.division_of_numbers(1, 0)))
print("4 / 3 (real) = " + str(pythonutils.division_of_numbers(4, 3, True)))
print("4 % 3 = " + str(pythonutils.modulo_of_numbers(4, 3)))
print("4! = " + str(pythonutils.factorial(4)))
print("digit sum of 482 = " + str(pythonutils.digit_sum(482)))
print("digit count of 482 = " + str(pythonutils.digit_count(482)))
print("digit count of 0 = " + str(pythonutils.digit_count(0)))
